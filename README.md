<div align="center">
<img src="./logo.png" width="300" >
<h1>Processo Seletivo 20.2</h1>
<h2>Exercícios HTML</h2>
<h2><a href="https://pksasso.gitlab.io/aula-html-in/">Live here</a></h2>
</div>

### Faça uma página pessoal contendo o seguinte:

### Página principal:
Cabeçalho com o título da página.
Uma foto qualquer.
Um parágrafo de apresentação falando um pouco sobre você. (mínimo de 50 palavras)
Lista não ordenada com 5 hobbies.
Lista ordenada com seus top 5 filmes favoritos (ou músicas, livros, séries, games, lugares,
etc..).
Rodapé contendo links para:
  - sua página do GitLab
  - suas redes sociais (Opcional)
  - página de contato.

### Página de contato:
Cabeçalho com o título da página.
Formulário contendo os seguintes campos: Nome, e-mail, telefone e campo para mensagem.
Botão para enviar a mensagem para seu endereço de e-mail.
Link para voltar à página principal.
Rodapé igual ao da página principal.

### Regras: 
1. Use apenas HTML.
2. A página principal e de contato devem somar no mínimo 20 tags diferentes.
3. O formulário de contato deve ser funcional (abrir e-mail padrão do usuário ao clicar no
botão enviar).
4. Faça bom uso de tags semânticas e comentários.

PS.: Não se atenha ao básico. Seja criativo! Pesquise como adicionar mais itens à página
(músicas, vídeos, tabelas, etc..).
Consulte outras tags aqui:
https://developer.mozilla.org/pt-BR/docs/Web/HTML/Element